package com.example.demo.services;

import java.util.ArrayList;


import java.util.List;


import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.example.demo.model.Student;
import com.example.demo.repository.StudentRepository;


@Service
@Transactional
public class StudentService {
	
	private final StudentRepository studentRepository;
	
	
	public StudentService(StudentRepository studentRepository)
	{
		this.studentRepository=studentRepository;
	}
	
	public void saveStudent(Student student)
	{
		studentRepository.save(student);
		
	}
	
	public List<Student> showAllStudents(){
		List<Student> students = new ArrayList<Student>();
		for(Student student: studentRepository.findAll()) {
			students.add(student);
		}
		
		return students;
	}

	public void deleteStudent(int id) {
		studentRepository.deleteById(id);
	}

	public Student editStudent(int id) {
		return studentRepository.findById(id);
	}
	
	public void updateStudent(int id,Student studentinfo)
	{
		Student student= studentRepository.findById(id);
		
		System.out.println("Age is "+studentinfo.getAge());
		
		student.setUsername(studentinfo.getUsername());
		student.setFirstname(studentinfo.getFirstname());
		student.setLastname(studentinfo.getLastname());
		student.setAge(studentinfo.getAge());
	
		
		
        final Student updatedStudent = studentRepository.save(student);

	}
	
}
