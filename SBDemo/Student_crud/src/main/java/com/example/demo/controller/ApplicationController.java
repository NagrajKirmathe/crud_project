package com.example.demo.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.model.Student;
import com.example.demo.services.StudentService;

@Controller
public class ApplicationController {
	
	
	@Autowired
	private StudentService studentService;
	
	
	@ResponseBody
	@RequestMapping("/home")
	public String Hello()
	{
		return "Hello From User";
	}
	
	@RequestMapping("/welcome")
	public String Welcome()
	{
		return "welcomepage";
	}
	
	@RequestMapping("/register")
	public String Register()
	{
		return "registerpage";
	}
	
	@PostMapping("/save-student")
	public String saveStudent(@RequestParam String username,@RequestParam String firstname,@RequestParam String lastname,@RequestParam int age,@RequestParam String password) 
	{
		
		System.out.print(firstname);
		Student student=new Student(username,firstname,lastname,age,password);
		studentService.saveStudent(student);
		return "registerpage";
		
	}
	
	@GetMapping("/show-students")
	public String showAllStudents(HttpServletRequest request) 
	{
		
		request.setAttribute("students", studentService.showAllStudents());
		return "studentlistpage";
		
	}
	
	@RequestMapping("/delete-student")
	public String deleteStudent(@RequestParam int id, HttpServletRequest request) {
		studentService.deleteStudent(id);
		request.setAttribute("students", studentService.showAllStudents());
		//request.setAttribute("mode", "ALL_USERS");
		return "studentlistpage";
	}
	
	
	@RequestMapping("/edit-student")
	public String editStudent(@RequestParam int id, HttpServletRequest request) {
		
		request.setAttribute("student", studentService.editStudent(id));
		//request.setAttribute("mode", "ALL_USERS");
		return "updatepage";
	}
	
	@PostMapping("/update-student")
	public String updateStudent(@RequestParam int id,@RequestParam String username,@RequestParam String firstname,@RequestParam String lastname,@RequestParam int age,HttpServletRequest request) 
	{
		System.out.println(id);
		Student studentinfo=new Student(username,firstname,lastname,age);
		studentService.updateStudent(id,studentinfo);
		request.setAttribute("students", studentService.showAllStudents());
		return "studentlistpage";
		
	}
	

}
