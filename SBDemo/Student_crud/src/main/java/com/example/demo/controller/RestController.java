package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.model.Student;
import com.example.demo.services.StudentService;

@org.springframework.web.bind.annotation.RestController
public class RestController 
{
	
	@Autowired
	private StudentService studentService;
	
	
//	@GetMapping("/")
//	public String hello()
//	{
//		return "This is home page";
//		
//	}

	
	@GetMapping("/save-student")
	public String saveUser(@RequestParam String username,@RequestParam String firstname,@RequestParam String lastname,@RequestParam int age,@RequestParam String password) 
	{
		
		Student student=new Student(username,firstname,lastname,age,password);
		studentService.saveStudent(student);
		return "user is saved";
		
	}
	
//	@RequestMapping("/demo")
//	public String Register()
//	{
//		return "demo";
//		
//	}
	
	
}
